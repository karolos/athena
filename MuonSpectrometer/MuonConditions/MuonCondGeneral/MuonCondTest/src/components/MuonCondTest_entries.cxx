/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "../AlignCondAthTest.h"
#include "../CSCConditionsTestAlgMT.h"
#include "../MDTConditionsTestAlg.h"
#include "../MDTConditionsTestAlgMT.h"
#include "../NswCondTestAlg.h"
#include "../NswPassivationTestAlg.h"
#include "../MdtCablingTestAlg.h"
#include "../RpcCablingTestAlg.h"

DECLARE_COMPONENT(AlignCondAthTest)
DECLARE_COMPONENT(MDTConditionsTestAlg)
DECLARE_COMPONENT(MDTConditionsTestAlgMT)
DECLARE_COMPONENT(CSCConditionsTestAlgMT)
DECLARE_COMPONENT(NswCondTestAlg)
DECLARE_COMPONENT(NswPassivationTestAlg)
DECLARE_COMPONENT(MdtCablingTestAlg)
DECLARE_COMPONENT(RpcCablingTestAlg)